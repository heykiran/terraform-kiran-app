terraform {
  required_providers {
    google = {
      source  = "hashicorp/google"
      version = "3.79.0"
    }
  }
}

module "sa" {
  source = "bitbucket.org/heykiran/terraform-sa?ref=1.0.0"
  env    = var.env
  name   = var.name
}

module "vm" {
  source   = "bitbucket.org/heykiran/terraform-vm?ref=1.0.0"
  env      = var.env
  name     = var.name
  sa_email = module.sa.sa_email
}

module "bucket" {
  source = "bitbucket.org/heykiran/terraform-bucket?ref=1.0.0"
  env    = var.env
  name   = var.name
}

resource "google_storage_bucket_iam_member" "bucket-iam" {
  bucket = module.bucket.bucket_name
  role   = "roles/storage.admin"
  member = "serviceAccount:${module.sa.sa_email}"
}