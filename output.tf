output "bucket_name" {
  value = module.bucket.bucket_name
}
output "sa_email" {
  value = module.sa.sa_email
}
output "vm_name" {
  value = module.vm.vm_name
}
output "vm_ip" {
  value = module.vm.vm_ip
}
