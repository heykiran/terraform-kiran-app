variable "env" {
  type        = string
  description = "environment name"
}

variable "name" {
  type        = string
  description = "name"
}